Maak een klasse Vector en een bijbehorende klasse VectorTest. Vul de
klasse Vector aan met onderstaande methoden. Voor een aantal van die
methoden heeft u de klasse Math nodig. Gebruik de API daarvan om de
juiste methoden van klasse Math te vinden. De code van de meeste
methoden is niet veel meer dan een paar regels.
Bij een aantal methoden moet u er wel rekening mee houden dat delen
door 0 niet zomaar kan.
Voorzie iedere methode van Javadoc commentaar en test in klasse
VectorTest iedere methode met behulp van JUnit. Test zowel positieve
gevallen als negatieve gevallen, en vergeet ook de waarde 0 niet.
