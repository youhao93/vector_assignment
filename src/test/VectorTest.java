package test;

import org.junit.jupiter.api.Test;
import vector.Vector;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {
private static final double EPSILON = 1e-15;

    @Test
    public void lengteVector() {
        Vector v1  = new Vector(3,4);
        assertEquals(5, v1.lengte(), EPSILON);

        Vector v2  = new Vector(8,0);
        assertEquals(8, v2.lengte(), EPSILON);

        Vector v3  = new Vector(0,8);
        assertEquals(8, v3.lengte(), EPSILON);

        Vector v4 = new Vector(-4, -3);
        assertEquals(5, v4.lengte(), EPSILON);

        Vector v5 = new Vector(-5, -3);
        assertEquals(5.830951894845301, v5.lengte(), EPSILON);

        Vector v6 = new Vector(0, 0);
        assertEquals(0, v6.lengte(), EPSILON);
    }


    @Test
    public void afstandVector() {
        Vector v1 = new Vector(3,4);
        Vector v2 = new Vector(5,7);
        assertEquals(3.605551275463989, v1.afstand(v2), EPSILON);

        Vector v3 = new Vector(5.56,8.91);
        Vector v4 = new Vector(2.10,3.30);
        assertEquals(6.591183505259128, v3.afstand(v4), EPSILON);

        Vector v5 = new Vector(0,8.91);
        Vector v6 = new Vector(2.10,0);
        assertEquals(9.154130215372732, v5.afstand(v6), EPSILON);

        Vector v7 = new Vector(-5,0);
        Vector v8 = new Vector(-3,4);
        assertEquals(4.47213595499958, v7.afstand(v8), EPSILON);

        Vector v9 = new Vector(-3,-4);
        Vector v10 = new Vector(-2,10);
        assertEquals(14.035668847618199, v9.afstand(v10), EPSILON);

        Vector v11 = new Vector(-2,0);
        Vector v12 = new Vector(0,8);
        assertEquals(8.246211251235321, v11.afstand(v12), EPSILON);
    }

    @Test
    public void plusVector() {
        Vector v1 = new Vector(3,4);
        Vector v2 = new Vector(1,-2);
        Vector plusVector = v1.plus(v2);
        assertEquals(4, plusVector.getX(), EPSILON);
        assertEquals(2, plusVector.getY(), EPSILON);

        Vector v3 = new Vector(2.930230,9.930120);
        Vector v4 = new Vector(5,8.030239);
        Vector plusVector2 = v3.plus(v4);
        assertEquals(7.930230, plusVector2.getX(), EPSILON);
        assertEquals(17.960359, plusVector2.getY(), EPSILON);

        Vector v5 = new Vector(0,5);
        Vector v6 = new Vector(3,0);
        Vector plusVector3 = v5.plus(v6);
        assertEquals(3, plusVector3.getX(), EPSILON);
        assertEquals(5, plusVector3.getY(), EPSILON);

        Vector v7 = new Vector(-5,6);
        Vector v8 = new Vector(3,-4);
        Vector plusVector4 = v7.plus(v8);
        assertEquals(-2, plusVector4.getX(), EPSILON);
        assertEquals(2, plusVector4.getY(), EPSILON);
    }

    @Test
    public void copyVector() {
        Vector v1 = new Vector(3,10);
        Vector copyVectorV1 = v1.copy();
        assertEquals(3, copyVectorV1.getX(), EPSILON);
        assertEquals(10, copyVectorV1.getY(), EPSILON);

        Vector v2 = new Vector(3.93910,10.201283);
        Vector copyVectorV2 = v2.copy();
        assertEquals(3.93910, copyVectorV2.getX(), EPSILON);
        assertEquals(10.201283, copyVectorV2.getY(), EPSILON);

        Vector v3 = new Vector(-5,-2);
        Vector copyVectorV3 = v3.copy();
        assertEquals(-5, copyVectorV3.getX(), EPSILON);
        assertEquals(-2, copyVectorV3.getY(), EPSILON);

        Vector v4 = new Vector(0,0);
        Vector copyVectorV4 = v4.copy();
        assertEquals(0, copyVectorV4.getX(), EPSILON);
        assertEquals(0, copyVectorV4.getY(), EPSILON);

        Vector v5 = new Vector(0,5);
        Vector copyVectorV5 = v5.copy();
        assertEquals(0, copyVectorV5.getX(), EPSILON);
        assertEquals(5, copyVectorV5.getY(), EPSILON);
    }

    @Test
    public void vectorGelijkAanVector() {
        /*
        Vectoren die precies dezelfde waardes hebben.
         */
        Vector v1 = new Vector(3,5);
        Vector v2 = new Vector(3,5);
        assertTrue(v1.equals(v2));

        /*
        Het verschil is groter dan 16 getallen achter het decimaal,
        waardoor deze vectoren, v3 en v4, niet aan elkaar gelijk zijn.
         */
        Vector v3 = new Vector(3.59203,4.590230000112333);
        Vector v4 = new Vector(3.59203,4.590230000112334);
        assertFalse(v3.equals(v4));

        /*
        Het verschil is hierbij NIET groter dan 16 getallen achter
        het decimaal, waardoor vector v5 en v6 aan elkaar gelijk zijn.
         */
        Vector v5 = new Vector(3.59203,4.5902300001123331);
        Vector v6 = new Vector(3.59203,4.5902300001123332);
        assertTrue(v5.equals(v6));

        /*
        Een vergelijking tussen een positief en negatief getal,
        waarvan de absolute waarde hetzelfde is.
         */
        Vector v7 = new Vector(-5.203029480302443,4);
        Vector v8 = new Vector(5.203029480302440,4);
        assertFalse(v7.equals(v8));

        /*
        Een vergelijking tussen 0-waardes.
         */
        Vector v9 = new Vector(0,0);
        Vector v10 = new Vector(0,0);
        assertTrue(v9.equals(v10));

        /*
        Een vergelijking waar bij de ene vector de y-waarde positief is en
        bij de andere negatief is.
         */
        Vector v11 = new Vector(0,5);
        Vector v12 = new Vector(0,-5);
        assertFalse(v11.equals(v12));

        Vector v13 = new Vector(0,5);
        Vector v14 = new Vector(4,0);
        assertFalse(v13.equals(v14));
    }

    @Test
    public void maalVector() {
        Vector v1 = new Vector(3,5);
        Vector maalVectorv1 = v1.maal(5);
        assertEquals(15, maalVectorv1.getX(), EPSILON);
        assertEquals(25, maalVectorv1.getY(), EPSILON);

        Vector v2 = new Vector(3.5,7.2);
        Vector maalVectorv2 = v2.maal(2);
        assertEquals(7, maalVectorv2.getX(), EPSILON);
        assertEquals(14.4, maalVectorv2.getY(), EPSILON);

        Vector v3 = new Vector(3.5,7.2);
        Vector maalVectorv3 = v3.maal(0);
        assertEquals(0, maalVectorv3.getX(), EPSILON);
        assertEquals(0, maalVectorv3.getY(), EPSILON);

        Vector v4 = new Vector(-3.5,0);
        Vector maalVectorv4 = v4.maal(3);
        assertEquals(-10.5, maalVectorv4.getX(), EPSILON);
        assertEquals(0, maalVectorv4.getY(), EPSILON);

        Vector v5 = new Vector(0,6);
        Vector maalVectorv5 = v5.maal(3);
        assertEquals(0, maalVectorv5.getX(), EPSILON);
        assertEquals(18, maalVectorv5.getY(), EPSILON);

        Vector v6 = new Vector(3,5);
        Vector maalVectorv6 = v6.maal(-3);
        assertEquals(-9, maalVectorv6.getX(), EPSILON);
        assertEquals(-15, maalVectorv6.getY(), EPSILON);

        Vector v7 = new Vector(3,5);
        Vector maalVectorv7 = v7.maal(-3.391039);
        assertEquals(-10.173117000000001, maalVectorv7.getX(), EPSILON);
        assertEquals(-16.955195, maalVectorv7.getY(), EPSILON);
    }

    @Test
    public void zelfdeRichtingVector() {
        /*
        De richtingen op x-as zijn hetzelfde (allebei positief).
         */
        Vector v1 = new Vector(2,0);
        Vector v2 = new Vector(10,0);
        assertTrue(v1.zelfdeRichting(v2));

        /*
        De richtingen op de y-as zijn hetzelfde (allebei negatief).
         */
        Vector v3 = new Vector(0,-2);
        Vector v4 = new Vector(0,-1);
        assertTrue(v3.zelfdeRichting(v4));

        /*
        De richtingen zijn niet hetzelfde, want vector v5 gaat schuin
        omhoog en vector v6 gaat schuin omlaag.
         */
        Vector v5 = new Vector(3,4);
        Vector v6 = new Vector(-3,-4);
        assertFalse(v5.zelfdeRichting(v6));

        /*
        Niet dezelfde richting, omdat vector v7 naar rechts gaat en
        vector v8 naar links.
         */
        Vector v7 = new Vector(2,0);
        Vector v8 = new Vector(-2,0);
        assertFalse(v7.zelfdeRichting(v8));

        /*
        Vector v9 gaat richting linksonder en vector v10 gaat naar
        rechtsboven.
         */
        Vector v9 = new Vector(-4,-2);
        Vector v10 = new Vector(4,2);
        assertFalse(v9.zelfdeRichting(v10));

        /*
        Vector v11 gaat omlaag en vector v12 gaat naar rechts.
         */
        Vector v11 = new Vector(0,-2);
        Vector v12 = new Vector(4,0);
        assertFalse(v11.zelfdeRichting(v12));

        /*
        Vector v13 gaat naar links terwijl vector v14 omlaag gaat.
         */
        Vector v13 = new Vector(-5,0);
        Vector v14 = new Vector(0,-10);
        assertFalse(v13.zelfdeRichting(v14));

        /*
        Allebei de vectoren gaan omhoog.
         */
        Vector v15 = new Vector(0,2);
        Vector v16 = new Vector(0,8);
        assertTrue(v15.zelfdeRichting(v16));


        Vector v19 = new Vector(3,4);
        Vector v20 = new Vector(4,2);
        assertFalse(v19.zelfdeRichting(v20));

        Vector v21 = new Vector(6,2);
        Vector v22 = new Vector(3,1);
        assertTrue(v21.zelfdeRichting(v22));

        Vector v23 = new Vector(3,6);
        Vector v24 = new Vector(2,4);
        assertTrue(v23.zelfdeRichting(v24));

        Vector v25 = new Vector(4,3);
        Vector v26 = new Vector(5,4);
        assertFalse(v25.zelfdeRichting(v26));

        Vector v27 = new Vector(0,0);
        Vector v28 = new Vector(0,0);
        assertFalse(v27.zelfdeRichting(v28));

        Vector v29 = new Vector(2,4);
        Vector v30 = new Vector(2,3);
        assertFalse(v29.zelfdeRichting(v30));
    }

    @Test
    public void inproduct() {
        Vector v1 = new Vector(3,5);
        Vector v2 = new Vector(3.5,7.2);
        assertEquals(46.5, v1.inproduct(v2), EPSILON);

        Vector v3 = new Vector(0,6);
        Vector v4 = new Vector(8,7.2);
        assertEquals(43.2, v3.inproduct(v4), EPSILON);

        Vector v5 = new Vector(-5,2);
        Vector v6 = new Vector(2,5);
        assertEquals(0, v5.inproduct(v6), EPSILON);

        Vector v7 = new Vector(0,2);
        Vector v8 = new Vector(0,5);
        assertEquals(10, v7.inproduct(v8), EPSILON);

        Vector v9 = new Vector(4,0);
        Vector v10 = new Vector(3,0);
        assertEquals(12, v9.inproduct(v10), EPSILON);

        Vector v11 = new Vector(4,0);
        Vector v12 = new Vector(3,-10);
        assertEquals(12, v11.inproduct(v12), EPSILON);

        Vector v13 = new Vector(9,0);
        Vector v14 = new Vector(0,-2);
        assertEquals(0, v13.inproduct(v14), EPSILON);

        Vector v15 = new Vector(0,-2);
        Vector v16 = new Vector(9,0);
        assertEquals(0, v15.inproduct(v16), EPSILON);

        Vector v17 = new Vector(-10,-25);
        Vector v18 = new Vector(-28,-12);
        assertEquals(580, v17.inproduct(v18), EPSILON);

        Vector v19 = new Vector(-10.83022309,-25);
        Vector v20 = new Vector(-28,-12.98203032);
        assertEquals(627.79700452, v19.inproduct(v20), EPSILON);
    }

    @Test
    public void hoek() {
        Vector v1 = new Vector(3,3);
        assertEquals((Math.PI /4), v1.hoek(), EPSILON);

        Vector v2 = new Vector(0, 4);
        assertEquals(Math.PI / 2, v2.hoek(), EPSILON);

        Vector v3 = new Vector(0, -4);
        assertEquals(-Math.PI/2, v3.hoek(), EPSILON);

        Vector v4 = new Vector(-4, 0);
        assertEquals(Math.PI, v4.hoek(), EPSILON);

        Vector v5 = new Vector(-3, 3);
        assertEquals(3 * Math.PI / 4, v5.hoek(), EPSILON);

        Vector v6 = new Vector(-3, -3);
        assertEquals(-3 * Math.PI / 4, v6.hoek(), EPSILON);

        Vector v7 = new Vector(-Math.sqrt(3)/2, -0.5);
        assertEquals(-5 * Math.PI / 6, v7.hoek(), EPSILON);

        Vector v8 = new Vector(-Math.sqrt(2)/2, -Math.sqrt(2)/2);
        assertEquals(-3 * Math.PI / 4, v8.hoek(), EPSILON);

        Vector v9 = new Vector(0, -1);
        assertEquals(-Math.PI/2, v9.hoek(), EPSILON);

        Vector v10 = new Vector(0.5, -Math.sqrt(3) / 2);
        assertEquals(-Math.PI / 3, v10.hoek(), EPSILON);

        Vector v11 = new Vector(Math.sqrt(2) / 2, -Math.sqrt(2) / 2);
        assertEquals(-Math.PI / 4, v11.hoek(), EPSILON);

        Vector v12 = new Vector(Math.sqrt(3) / 2, -0.5);
        assertEquals(-Math.PI / 6, v12.hoek(), EPSILON);

        Vector v13 = new Vector(-0.5, -Math.sqrt(3) / 2);
        assertEquals(-2 * Math.PI / 3, v13.hoek(), EPSILON);

        Vector v14 = new Vector(-Math.sqrt(2) / 2, Math.sqrt(2) / 2);
        assertEquals(3 * Math.PI / 4, v14.hoek(), EPSILON);

        Vector v15 = new Vector(-0.5, Math.sqrt(3) / 2);
        assertEquals(2 * Math.PI / 3, v15.hoek(), EPSILON);

        Vector v16 = new Vector(4, 2);
        assertEquals(0.4636476090008061, v16.hoek(), EPSILON);

        Vector v17 = new Vector(-3, 5);
        assertEquals(2.1112158270654806, v17.hoek(), EPSILON);

        Vector v18 = new Vector(-3, -8);
        assertEquals(-1.929566997065469, v18.hoek(), EPSILON);

        Vector v19 = new Vector(3, -2);
        assertEquals(-0.5880026035475675, v19.hoek(), EPSILON);
    }

}


