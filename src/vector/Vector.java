package vector;

/**
 * Klasse die verantwoordelijk is voor het berekenen van vectoren. Dit betreft 
 * berekeningen van verschillende wiskundige vraagstukken die zijn opgesplitst per
 * methode.
 *
 * 
 * @author youhao
 */

public class Vector {
    private double x;
    private double y;
    private static final double EPSILON = 1e-16;

    /**
     * De constructor die objecten vectoren aanmaakt met twee objectattributen.
     * 
     * @param x is de x-waarde van de vector.
     * @param y is de y-waarde van de vector.
     */
    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Berekent de lengte (tot twee decimalen) van de vector die deze methode aanroept.
     * 
     * @return de berekende lengte van twee vectoren die is afgerond op twee decimalen.
     */
    public double lengte() {
        //DecimalFormat decimal = new DecimalFormat("##.##");
        return Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2)));
    }

    /**
     * Berekent de afstand tussen twee vectoren, namelijk de vector die 
     * deze methode aanroept en de vector die als argument in deze methode is geplaatst.
     * 
     * @param v is de vector die in de methode geplaatst wordt.
     * @return  de berekende afstand tussen twee vectoren, welke is afgerond op twee decimalen.
     */
    public double afstand(Vector v) {
        return Math.sqrt(Math.pow((this.x - v.x), 2) + Math.pow((this.y - v.y), 2));
    }

    /**
     * Berekent de som over twee vectoren, waarbij de som x coordinaat de som is van
     * de x coordinaten en de som y coordinaat de som is van de y coordinaten. Bij elkaar
     * vormt de som x coordinaat en de som y coordinaat de nieuwe somvector.
     * 
     * @param v is de vector die in de methode wordt geplaatst
     * @return de nieuwe vector, waarbij de x en y coordinaten de sommen zijn van
     * 			de twee vectoren.
     */
    public Vector plus(Vector v) {
        double somX = this.x + v.x;
        double somY = this.y + v.y;
        return new Vector(somX, somY);
    }

    /**
     * Maakt een exacte kopie van de x en y coordinaat van de vector die deze methode aanroept.
     * 
     * @return de exacte kopie van de vector die deze methode aanroept.
     */
    public Vector copy() {
        return new Vector(this.x, this.y);
    }

    /**
     * Vergelijkt twee vectoren of de x en y coordinaten aan elkaar gelijk zijn als
     * absolute waarde. De verschillen vallen weg als die kleiner zijn dan EPSILON (1e-16).
     * 
     * @param v ingegeven vector die vergeleken wordt met de vector die deze methode aanroept
     * @return de waarde of twee vectoren wel (true) aan elkaar gelijk zijn of niet (false) 
     * 			van de twee vectoren.
     */
    public boolean equals(Vector v) {
        double x1 = this.x;
        double x2 = v.x;
        double y1 = this.y;
        double y2 = v.y;
        return ((Math.abs(x1 - x2) < EPSILON) && (Math.abs(y1 - y2) < EPSILON));
    }

    /**
     * Een vector, waarbij de x en y coordinaten van die vector worden vermenigvuldigd.

     * @param d is het cijfer waarbij x en y coordinaten mee vermenigvuldigt worden.
     * @return de nieuwe vector met de vermenigvuldigde x en y coordinaten door getal 'd'.
     */
    public Vector maal(double d) {
        double nieuweX = x * d;
        double nieuweY = y * d;
        return new Vector (nieuweX, nieuweY);
    }

    /**
     * Geeft aan of twee vectoren dezelfde richting hebben.
     * 
     * @param v is de parameter die in de methode wordt geplaatst en
     *          wordt vergeleken met de vector die deze methode aanroept.
     * @return de waarde die aangeeft of de richting hetzelfde is tussen
     * twee vectoren (true) of niet (false).
     */
    public boolean zelfdeRichting(Vector v) {

        double g = 0;
        double g2 = 0;

        if(this.y == 0 && v.getY() == 0) {
            if((this.x > 0 && v.getX() > 0) || (this.x < 0 && v.getX() < 0)) {
                return true;
            }
        }

        if(this.x == 0 && v.getX() == 0) {
            if((this.y > 0 && v.getY() > 0) || (this.y < 0 && v.getY() < 0)) {
                return true;
            }
        }

        g = this.x / v.getX();
        g2 = this.y / v.getY();
        if (g == g2 && g > 0 && g2 > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Berekent het inwendig product van twee vectoren, door de lengte van de ene 
     * vector te vermenigvuldigen met de lengte van de andere vector.
     * 
     * @param v is de vector die met de vector die de methode aanroept vermenigvuldigt wordt.
     * @return de berekende double waarde van het inwendig product van de twee vectoren.
     */
    public double inproduct(Vector v) {
        return (this.x * v.getX()) + (this.y * v.getY());
    }

    /**
     * Berekent de hoek met de x as van de vector die deze methode aanroept.
     * Als er gedeeld wordt door een 0, dan wordt een NaN aan variabele tan toegekend.
     *
     * Eerst wordt de tan berekend door y te delen met x. Vervolgens wordt Math.atan
     * toegepast die het resultaat van tan omzet in radialen.
     * 
     * @return de hoek in radialen.
     */
    public double hoek() {
            return Math.atan2(this.y, this.x);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}


